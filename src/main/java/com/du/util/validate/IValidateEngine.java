package com.du.util.validate;



public interface IValidateEngine {

	void doValidate() throws ValidateFailException;
	void addValidateRule(IValidateRule rule);
	void clearRules();
	IValidateContext getContext();
}
