package com.du.util.validate;

public class ValidateFailException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2762131383683156761L;
	public ValidateFailException(String error){
		super(error);
	}
	
	@Override
    public Throwable fillInStackTrace()
    {
        return this;
    }

}
