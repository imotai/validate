package com.du.util.validate;

public abstract class AbstractValidateRule implements IValidateRule {

	/**
	 * context 只能在validate 函数中使用
	 */
	protected IValidateContext context;
	@Override
	public abstract void validate() throws ValidateFailException ;
	@Override
	public void addContext(IValidateContext context) {
		this.context = context;
	}

}
