package com.du.util.validate;

public interface IValidateContext {

	void add(String key,Object object);
	void remove(String key);
	Object get(String key);
	void clear();
}
