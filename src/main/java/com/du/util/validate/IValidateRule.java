package com.du.util.validate;

public interface IValidateRule {

	
	void validate() throws ValidateFailException;
	void addContext(IValidateContext context);
}
