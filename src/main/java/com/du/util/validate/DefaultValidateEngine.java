package com.du.util.validate;

import java.util.LinkedList;
public class DefaultValidateEngine implements IValidateEngine {

	private LinkedList<IValidateRule> rules = new LinkedList<IValidateRule>();
	private IValidateContext context ;
	public DefaultValidateEngine(){
		context = new MapValidateContext();
	}
	public void doValidate() throws ValidateFailException {
		IValidateRule rule = rules.poll();
		while(rule!=null){
			rule.addContext(context);
			rule.validate();
			rule = rules.poll();
		}
	}
	public void addValidateRule(IValidateRule rule) {
		if(rule==null){
			return;
		}
		rules.add(rule);
	}
	public void clearRules() {
		if(rules.isEmpty()){
			return;
		}
		rules.clear();
	}
	public IValidateContext getContext(){
		return this.context;
	}
}
