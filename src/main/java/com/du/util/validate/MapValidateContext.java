package com.du.util.validate;

import java.util.HashMap;
import java.util.Map;

public class MapValidateContext implements IValidateContext {

	private Map<String,Object> context ;
	protected MapValidateContext(){
		context = new HashMap<String,Object>();
	}
	@Override
	public void add(String key, Object object) {
		this.context.put(key, object);
	}

	@Override
	public void remove(String key) {
		this.remove(key);
	}
	@Override
	public Object get(String key) {
		return this.context.get(key);
	}
	@Override
	public void clear() {
		this.context.clear();
	}

}
